export interface CatModel {
    id        : string;
    name      : string;
    origin    : string;
}