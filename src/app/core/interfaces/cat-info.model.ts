export interface CatInfoModel {
    id              : string;
    name            : string;
    description     : string;
    wikipedia_url   : string;
    idImage         ?:string;
}

export interface ImageModel {
    id              :string;
    url             :string;
}