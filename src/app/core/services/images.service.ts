import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ImageModel } from '../interfaces/cat-info.model';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {
  private path = 'https://api.thecatapi.com/v1/images';

  constructor( private http: HttpClient ) { }

  getImg(id: string) {
    return this.http.get(`${this.path}/${id}`).pipe (
      map ( this.gData )
    );
  }

  private gData( obj: any) {

    if (obj === undefined || obj === null) return [];

    let img: ImageModel = {
      id: obj.id,
      url: obj.url
    }

    return img;
  }
}
