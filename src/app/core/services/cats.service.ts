import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {map} from "rxjs/operators";
import { CatModel } from '../interfaces/cat.model';
import { CatInfoModel } from '../interfaces/cat-info.model';

@Injectable({
  providedIn: 'root'
})
export class CatsService {
  private path = 'https://api.thecatapi.com/v1/breeds';
  private pathSearch = 'https://api.thecatapi.com/v1/breeds/search';

  constructor(private http: HttpClient ) { }

  getCats(){
    return this.http.get(`${this.path}`).pipe(
        map ( this.arrayCats )
      );
  }

  getCat( id: string ){
    const params = new HttpParams()
      .set('q', id);
    return this.http.get(`${this.pathSearch}`, { params }).pipe(
        map ( this.gCat )
      );
  }

  private gCat( cat: any ) {
    if (cat.length === 0 ) { return null; }
    let _cat: CatInfoModel = {
      id: cat[0].id,
      description: cat[0].description,
      name: cat[0].name,
      wikipedia_url: cat[0].wikipedia_url,
      idImage: cat[0].reference_image_id
    }

    return _cat;
  }

  private arrayCats( cats: object ){
    const _cats: CatModel[] = [];

    if (cats === null ) { return []; }

    Object.keys(cats).forEach( key => {
      const cat : CatModel = {
        name : cats[key].name,
        origin: cats[key].origin,
        id: cats[key].id
      }
      _cats.push(cat);
    } );

    return _cats;
  }
}
