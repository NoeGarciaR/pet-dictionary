import { HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor() { }

  intercept( req: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {
    const headers = new HttpHeaders({
      'x-api-key': 'd38e5bd5-97b7-4c60-af0b-6bc2157e4e56'
    });

    const reqClone = req.clone({
      headers
    });

    return next.handle(reqClone).pipe(
      catchError(this.errorMessage)
    );
  }

  errorMessage( error: HttpErrorResponse ) {
    console.warn(error);
    return throwError('Error en el servicio');
  }
}
