import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CatsComponent } from './components/cats/cats.component';
import { CatComponent } from './components/cat/cat.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'cats', component: CatsComponent},
  { path: 'cat/:id', component: CatComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
