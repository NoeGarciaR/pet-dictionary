import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CatsService } from '../../core/services/cats.service';
import { ImagesService } from '../../core/services/images.service';
import { CatModel } from '../../core/interfaces/cat.model';
import { CatInfoModel, ImageModel } from '../../core/interfaces/cat-info.model';

@Component({
  selector: 'app-cat',
  templateUrl: './cat.component.html',
  styleUrls: ['./cat.component.scss']
})
export class CatComponent implements OnInit {
  cat: CatInfoModel;
  image: ImageModel;
  error = false;

  constructor( private catService : CatsService,
               private imageService: ImagesService,
                private route: ActivatedRoute
     ) { 
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.catService.getCat(id).subscribe( (res: CatInfoModel) => {
      this.cat = res;
      if(res){
        this.imageService.getImg(res.idImage).subscribe( (res: ImageModel) => {
          this.image = res;
        })
      } else {
        this.error = true;
      }
    });
  }

}