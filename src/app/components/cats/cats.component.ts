import { Component, OnInit, PipeTransform } from '@angular/core';
import { CatsService } from '../../core/services/cats.service';
import { CatModel } from '../../core/interfaces/cat.model';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'app-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.scss']
})

export class CatsComponent implements OnInit {
  cats: CatModel[] = [];

  showCats$: Observable<CatModel[]>;
  filter = new FormControl('');

  constructor( private catService: CatsService
     ) {
      }

  ngOnInit(): void {
    this.catService.getCats().subscribe( (res: CatModel[]) => {
      this.cats = res;
      this.showCats$ = this.filter.valueChanges.pipe(
        startWith(''),
        map(text => this.search(text))
      );
    }
    );
  }


  search(text: string): CatModel[] {
    return this.cats.filter(cat => {
      const term = text.toLowerCase();
      return cat.name.toLowerCase().includes(term)
          || cat.origin.toLowerCase().includes(term);
    });
  }
}
